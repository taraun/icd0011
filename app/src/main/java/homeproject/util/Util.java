package homeproject.util;

import java.io.InputStream;
import java.nio.charset.StandardCharsets;
import java.util.HashMap;
import java.util.Map;
import java.util.Scanner;

public class Util {

    public static String readStream(InputStream is) {
        try (Scanner scanner = new Scanner(is,
                StandardCharsets.UTF_8.name()).useDelimiter("\\A")) {

            return scanner.hasNext() ? scanner.next() : "";
        }
    }

    public static Map<String, String> parseJson(String str) {
        Map<String, String> map = new HashMap<>();
        str = str
                .replace("{", " ")
                .replace("}", " ")
                .replace("\"", " ")
                .trim();

        for (String keyValue: str.split(",")) {
            String[] data = keyValue.split(":");
            map.put(data[0].trim(), data[1].trim());
        }

        return map;
    }

    public static Map<String, String> parseText(String str) {
        Map<String, String> map = new HashMap<>();

        for (String keyValue: str.split("&")) {
            String[] data = keyValue.split("=");
            map.put(data[0].trim(), data[1].trim());
        }

        return map;
    }

}
