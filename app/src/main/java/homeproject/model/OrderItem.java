package homeproject.model;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.validation.constraints.Min;
import javax.validation.constraints.NotNull;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class OrderItem {
    private Integer id;
    private Integer orderId;

    @NotNull
    @Min(1)
    private Integer quantity;

    private String itemName;

    @NotNull
    @Min(1)
    private Float price;
}
