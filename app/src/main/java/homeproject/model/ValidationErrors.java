package homeproject.model;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

public class ValidationErrors {

    private List<HashMap<String, String>> errors = new ArrayList<>();

    public List<HashMap<String, String>> getErrors() {
        return errors;
    }

    public void addError(String key, String value) {
        HashMap<String, String> error = new HashMap<>();
        error.put(key, value);
        this.errors.add(error);
    }

    public boolean isErrors () {
        return !errors.isEmpty();
    }
}
