package homeproject.model;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.validation.Valid;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import java.util.ArrayList;
import java.util.List;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class Order {
    private Integer id;

    @NotNull
    @Size(min = 2, max = 10)
    private String orderNumber;

    @Valid
    private List<OrderItem> orderRows;

    public void addItems(OrderItem item) {
        if (orderRows == null) {
            orderRows = new ArrayList<>();
        }
        orderRows.add(item);
    }
}
