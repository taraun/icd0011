package homeproject.dao;

import homeproject.model.Order;
import homeproject.model.OrderItem;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.jdbc.core.namedparam.BeanPropertySqlParameterSource;
import org.springframework.jdbc.core.simple.SimpleJdbcInsert;
import org.springframework.stereotype.Repository;

import java.sql.*;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

@Repository()
public class OrderDao {
    private JdbcTemplate template;

    private String tableName = "orders";
    private String tableNameItems = "order_items";
    private String allSelectFields = "orders.id, orders.order_number, item.id as itemId, " +
            "item.name AS itemName, item.quantity, item.price";
    private String allFieldsQuery = String.format("select "+ allSelectFields +" from %s " +
            "left join %s as item on orders.id = item.order_id ", tableName, tableNameItems);

    public OrderDao(JdbcTemplate template) {
        this.template = template;
    }

    public List<Order> find() {
        String sql = allFieldsQuery;
        List<Order> allOrders = template.query(sql, new OrderMapper());

        return getGroupedOrders(allOrders);
    }

    public Order findById(Integer id) {
        String sql = allFieldsQuery + "where orders.id = ?";

        List<Order> allOrdersById = template.query(sql, new OrderMapper(), id);
        List<Order> orders = getGroupedOrders(allOrdersById);

        if (orders.size() == 1) {
            return orders.get(0);
        }
        return null;
    }

    public Order insert(Order order) {
        List<OrderItem> items = order.getOrderRows();
        order.setOrderRows(null);

        var data = new BeanPropertySqlParameterSource(order);

        Number number = new SimpleJdbcInsert(template)
                .withTableName("orders")
                .usingGeneratedKeyColumns("id")
                .executeAndReturnKey(data);

        order.setId(number.intValue());

        if (items == null || items.isEmpty()) {
            return order;
        }

        String sqlItem = String.format("insert into %s (order_id, name, quantity, price) values(?, ?, ?, ?)", tableNameItems);
        List<Object[]> batchArgsList = new ArrayList<>();

        for (OrderItem item: items) {
            Object[] objectArray = { order.getId(),
                    item.getItemName(), item.getQuantity(), item.getPrice() };
            batchArgsList.add(objectArray);
        }
        template.batchUpdate(sqlItem, batchArgsList);

        return findById(order.getId());
    }

    public void delete(Integer orderId) {
        String sql = String.format("DELETE FROM %s WHERE id = ?", tableName);

        template.update(sql, orderId);
    }


    private List<Order> getGroupedOrders(List<Order> nonGroupedOrders) {
        List<Order> orders = new ArrayList<>();
        HashMap<Integer, Order> orderHashMap = new HashMap<>();

        for (Order order: nonGroupedOrders) {
            Order existingOrder = orderHashMap.get(order.getId());

            if (existingOrder == null) {
                orders.add(order);
                orderHashMap.put(order.getId(), order);
            } else {
                OrderItem item = order.getOrderRows().get(0);
                existingOrder.addItems(item);
            }
        }

        return orders;
    }

    private static class OrderMapper implements RowMapper<Order> {
        @Override
        public Order mapRow(ResultSet rs, int rowNum) throws SQLException {

            Order order = new Order();
            order.setId(rs.getInt("id"));
            order.setOrderNumber(rs.getString("order_number"));

            OrderItem item = new OrderItem(
                    rs.getInt("itemId"),
                    rs.getInt("id"),
                    rs.getInt("quantity"),
                    rs.getString("itemname"),
                    rs.getFloat("price"));
            order.addItems(item);

            return order;
        }
    }
}
