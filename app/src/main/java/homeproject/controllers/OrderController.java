package homeproject.controllers;

import homeproject.dao.OrderDao;
import homeproject.model.Order;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.List;

@RestController
public class OrderController {
    private OrderDao orderDao;

    public OrderController(OrderDao orderDao) {
        this.orderDao = orderDao;
    }

    @GetMapping("orders")
    public List<Order> getOrders() {
        return this.orderDao.find();
    }

    @GetMapping("orders/{id}")
    public Order getSingleOrder(@PathVariable Integer id) {
        return this.orderDao.findById(id);
    }

    @PostMapping("orders")
    protected Order saveOrder(@RequestBody @Valid Order order) {
        return this.orderDao.insert(order);
    }

    @DeleteMapping("orders/{id}")
    protected void deleteOrder(@PathVariable Integer id) {
        this.orderDao.delete(id);
    }
}
