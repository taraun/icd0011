package homeproject.config;

import org.springframework.context.annotation.*;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.web.servlet.config.annotation.EnableWebMvc;

import javax.sql.DataSource;

@EnableWebMvc
@Configuration
@PropertySource("classpath:/application.properties")
@ComponentScan(basePackages = { "homeproject.controllers" })
@ComponentScan(basePackages = { "homeproject.dao" })
public class Config {

    @Bean()
    public JdbcTemplate getTemplate(DataSource ds) {
        return new JdbcTemplate(ds);
    }
}